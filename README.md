# py3_desglose

Este es un script para hacer desgloses de planillas.

Toma un archivo CSV con una lista de nombres y montos a pagar.

Produce un archivo CSV con la lista de nombres y la cantidad de billetes (y monedas) de cada denominación para completar el pago de esa persona.

Como salida de pantalla produce la suma total de billetes (y monedas) de cada denominación para hacer el pago de la planilla total.

Es una labor simple, pero tediosa.
