#!/usr/bin/python3
# -*- coding: utf-8 -*-

from decimal import * #Decimal #, normalize
# getcontext().prec = 2

denominacion = [500, 200, 100, 50, 20, 10, 5, 1, 0.5, 0.25, 0.10, 0.05, 0.01]
billetes = 10  # valor del billete de menor denominacion

planilla_file = open("sample.csv", "r")
planilla_list = planilla_file.readlines()
planilla_file.close()

desglose = {}

for i in planilla_list:
    empleado = i.strip()
    empleado = empleado.split(",")
    empleado_actual = empleado[0]
    if empleado_actual not in desglose:
        desglose[empleado_actual] = {}
    monto_pagar = empleado[1]
    desglose[empleado_actual]["a pagar"] = Decimal(monto_pagar).quantize(Decimal('.01'))

# print(desglose)

for empleado in list(desglose.keys()):
    pagar = Decimal(desglose[empleado]["a pagar"]).quantize(Decimal('.01'))
    for d in denominacion:
        if pagar > d:
            cantidad = Decimal(pagar).quantize(Decimal('.01')) // Decimal(d).quantize(Decimal('.01'))
            # pagar -= cantidad * Decimal(d).quantize(Decimal('.01'))
            pagar = Decimal(pagar).quantize(Decimal('.01')) % Decimal(d).quantize(Decimal('.01'))
        elif pagar == d:
            cantidad = Decimal(1).quantize(Decimal('.01'))
            pagar -= cantidad
        else:
            cantidad = Decimal(0).quantize(Decimal('.01'))
        desglose[empleado][d] = Decimal(cantidad)

print(desglose)

resumen = {}

for empleado in list(desglose.keys()):
    for d in denominacion:
        if d not in resumen:
            resumen[d] = 0
        resumen[d] += desglose[empleado][d]

# print(resumen)

print ("Resumen del desglose")
total = 0
for d in denominacion:
    sub_total = resumen[d] * Decimal(d)
    if d >= billetes:
        print(resumen[d], "Billetes de", d, "=", Decimal(sub_total).quantize(Decimal('.01')))
    elif d >= 1:
        print(resumen[d], "Monedas de", d, "=", Decimal(sub_total).quantize(Decimal('.01')))
    else:
        print(resumen[d], "Monedas de", int(d * 100), "=", Decimal(sub_total).quantize(Decimal('.01')))
    total += sub_total
print("Total =", Decimal(total).quantize(Decimal('.01')))
output = open("output.csv", "w")
print("Formato de Sobres,,,,,,,,,,,,,,", file=output)
print(("Empleado"), end=",", file=output)
for d in denominacion:
    print(d, end= ",", file=output)
print("", file=output)
for empleado in list(desglose.keys()):
    print((empleado), end=',', file=output)
    for d in denominacion:
        print(desglose[empleado][d], end = ",", file=output)
    print("", file=output)
output.close()
